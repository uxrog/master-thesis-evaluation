generate-batch-strong-cpu:
	python scripts/substitute.py strong-batch.j2 < strong-cpu.json

generate-batch-strong-gpu:
	python scripts/substitute.py strong-batch.j2 < strong-gpu.json

generate-batch-strong-booster:
	python scripts/substitute.py strong-batch.j2 < strong-booster.json

clean-logs:
	rm logs/*

.PHONY: generate-batch-strong-cpu clean-logs
