# How to profile CUDA in NAStJA

## Kernels

(works for a single rank only but can be tweaked for multiple)

```
# --disable-dcgm disabled JUWELS performance counters for the GPUs so that ncu can access them
salloc --partition develbooster --account hkf6 --nodes 1 --ntasks 1 --time 01:00:00 --disable-dcgm
# For profiling kernels you likely want a config that only runs a single CUDA sweep because ncu already samples them multiple times
srun --pty ncu --set full --kernel-name updateDynamicECMKernel -o data/profile path-to-cuda-compiled/nastja -c profiling/strong-1step.json
```

Note that this will profile all templated versions of `updateDynamicECMKernel` too.
