# Master's Thesis Evaluation

This is a loose collection of scripts used for the performance evaluation of my master's thesis.

## Dependencies

Just the Python standard library and `pandas`.
On JUWELS, you can `source load-modules` to load the SciPy suite which includes `pandas`.
Nobody's stopping you from using `pip` either.

## Scripts

| Path                                           | Description                                                                                                                                                                                             |
| ---------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `./scripts/generate-strong-cpu-configs.py`     | Generates NAStJA configurations for CPU implementation strong scaling measurements.                                                                                                                     | 
| `./scripts/generate-strong-gpu-configs.py`     | Generates NAStJA configurations for GPU implementation strong scaling measurements on JUWELS Cluster.                                                                                                   |
| `./scripts/generate-strong-booster-configs.py` | Generates NAStJA configurations for GPU implementation strong scaling measurements on JUWELS Booster.                                                                                                   |
| `./scripts/generate-weak-configs.py`           | Generates NAStJA configurations and slurm batch files for weak scaling measurements.                                                                                                                    |
| `./scripts/generate-subblock-configs.py`       | Generates NAStJA configurations and slurm batch files for subblock size measurements.                                                                                                                   |
| `./scripts/generate-rank-configs.py`           | Generates NAStJA configurations and slurm batch files for measurements with varying ranks using a single GPU.                                                                                           |
| `./scripts/generate-fill-configs.py`           | Generates NAStJA configurations and slurm batch files for measurements with varying ECM filling.                                                                                                        |
| `./scripts/timing.py`                          | Collects mean NAStJA timing information and slurm accounting data from `/p/scratch/cellsinsilico/paul/nastja-out` for given slurm job IDs and formats them as JSON.                                     |
| `./scripts/timing-actions.py`                  | Collects mean NAStJA timing information and slurm accounting data from `/p/scratch/cellsinsilico/paul/nastja-out` for given slurm job IDs and formats them as CSV, includes timing data by action name. |
| `./scripts/build-pgf-datafiles.py`             | Build `.tsv` files from JSON timing files that can be read by LaTeX `pgfplots`.                                                                                                                         |
| `./scripts/make-latex-table.py`                | Generate LaTeX table rows from JSON timing files.                                                                                                                                                       |

Some scripts are supposed to be used by the `Makefile`, read that to see the available targets.