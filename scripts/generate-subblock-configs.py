#!/usr/bin/env python


import importlib
import itertools
import jinja2
import json
import sys


from pathlib import Path
from typing import Tuple


config_template_path = Path(__file__).parent.parent / "templates" / "strong-booster.json"
generated_path = Path(__file__).parent.parent / "generated"

templates_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(Path(__file__).parent.parent / "templates"),
    autoescape=jinja2.select_autoescape()
)

subblock_sizes = list(itertools.chain.from_iterable([
    [(1, 1, 1)],
    [(10, 8, 6)],
    [(i, 1, 1) for i in range(2, 66, 2)],
    [(30, i, 1) for i in [6]],
    [(14, i, 1) for i in [6, 14, 18]],
    [(6, i, 1) for i in [6, 14]],
    [(14, 6, i) for i in [6]],
]))


def make_label(subblock_size: Tuple[int, int, int]) -> str:
    return f"subblock-{subblock_size[0]:02}-{subblock_size[1]:02}-{subblock_size[2]:02}"


def write_config(subblock_size: Tuple[int, int, int]) -> None:
    with config_template_path.open(encoding="utf8") as f:
        config = json.load(f)

    config["Geometry"]["blockcount"] = [1, 1, 1]
    config["Geometry"]["blocksize"] = [384, 384, 384]
    config["Settings"]["cuda"]["subblock"]["blockDim"] = list(subblock_size)

    out_path = (generated_path / "config" / make_label(subblock_size)).with_suffix(".json")
    print(f"Writing config to {out_path}", file=sys.stderr)
    with out_path.open("w", encoding="utf8") as f:
        json.dump(config, f)


def write_batch(subblock_size: Tuple[int, int, int]) -> None:
    out_path = generated_path / "batch" / make_label(subblock_size)
    config_path = (generated_path / "config" / make_label(subblock_size)).with_suffix(".json")
    print(f"Writing batch to {out_path}", file=sys.stderr)

    t = templates_env.get_template("strong-batch.j2")
    t.stream(
        name=make_label(subblock_size),
        account="hkf6",
        partition="booster",
        nodes=1,
        tasks=1,
        extra_sbatch_line="#SBATCH --gres=gpu:1",
        time="00:15:00",
        nastja_binary_path="/p/project/cellsinsilico/paulslustigebude/nastja/build-cuda/nastja",
        logfile_path="/p/project/cellsinsilico/paulslustigebude/ma/experiments/eval/logs/%x-%A.%a",
        config_path="/p/project/cellsinsilico/paulslustigebude/ma/experiments/eval/generated/config/${SLURM_JOB_NAME}.json",
        output_dir_path="/p/scratch/cellsinsilico/paul/nastja-out/${SLURM_JOB_NAME}-${SLURM_ARRAY_JOB_ID}.${SLURM_ARRAY_TASK_ID}"
    ).dump(str(out_path))

if __name__ == "__main__":
    for subblock_size in subblock_sizes:
        required_shared_memory = (subblock_size[0] + 2) * (subblock_size[1] + 2) * (subblock_size[2] + 2) * 22 * 4
        print(f"{make_label(subblock_size)} will require {required_shared_memory:,}B of shared memory", file=sys.stderr)

        write_config(subblock_size)
        write_batch(subblock_size)
