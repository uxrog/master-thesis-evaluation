#!/usr/bin/env python

import jinja2
import json
import sys

from pathlib import Path

data = json.load(sys.stdin)

templates_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(Path(__file__).parent.parent / "templates"),
    autoescape=jinja2.select_autoescape()
)

for possibly_incomplete_batch in data["batches"]:
    batch = dict(list(data["common"].items()) + list(possibly_incomplete_batch.items()))
    out_path = Path(__file__).parent.parent / "generated" / "batch" / batch["name"]
    t = templates_env.get_template(sys.argv[1])
    print(f"Dumping to {out_path}")
    t.stream(**batch).dump(str(out_path))
