#!/usr/bin/env python


import importlib
import itertools
import jinja2
import json
import sys


from pathlib import Path
from typing import Tuple


config_template_path = Path(__file__).parent.parent / "templates" / "strong-booster.json"
generated_path = Path(__file__).parent.parent / "generated"

templates_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(Path(__file__).parent.parent / "templates"),
    autoescape=jinja2.select_autoescape()
)


SIZE = (384, 384, 384)
block_counts = [
    (1, 1, 1),
    (1, 1, 3),
    (1, 2, 3),
    (2, 2, 3),
    (2, 4, 3),
    (4, 4, 3),
]


def make_label(block_count: Tuple[int, int, int]) -> str:
    return f"rank-{block_count[0]:02}-{block_count[1]:02}-{block_count[2]:02}"


def write_config(block_count: Tuple[int, int, int]) -> None:
    with config_template_path.open(encoding="utf8") as f:
        config = json.load(f)

    config["Geometry"]["blockcount"] = list(block_count)
    assert SIZE[0] % block_count[0] == 0
    assert SIZE[1] % block_count[1] == 0
    assert SIZE[2] % block_count[2] == 0
    config["Geometry"]["blocksize"] = [SIZE[0] // block_count[0], SIZE[1] // block_count[1], SIZE[2] // block_count[2]]
    config["Settings"]["cuda"]["subblock"]["blockDim"] = [10, 8, 6]

    out_path = (generated_path / "config" / make_label(block_count)).with_suffix(".json")
    print(f"Writing config to {out_path}", file=sys.stderr)
    with out_path.open("w", encoding="utf8") as f:
        json.dump(config, f)


def write_batch(block_count: Tuple[int, int, int]) -> None:
    out_path = generated_path / "batch" / make_label(block_count)
    config_path = (generated_path / "config" / make_label(block_count)).with_suffix(".json")
    print(f"Writing batch to {out_path}", file=sys.stderr)

    t = templates_env.get_template("strong-batch.j2")
    t.stream(
        name=make_label(block_count),
        account="hkf6",
        partition="booster",
        nodes=1,
        tasks=block_count[0] * block_count[1] * block_count[2],
        extra_sbatch_line="#SBATCH --gres=gpu:1",
        time="00:15:00",
        nastja_binary_path="/p/project/cellsinsilico/paulslustigebude/nastja/build-cuda/nastja",
        logfile_path="/p/project/cellsinsilico/paulslustigebude/ma/experiments/eval/logs/%x-%A.%a",
        config_path="/p/project/cellsinsilico/paulslustigebude/ma/experiments/eval/generated/config/${SLURM_JOB_NAME}.json",
        output_dir_path="/p/scratch/cellsinsilico/paul/nastja-out/${SLURM_JOB_NAME}-${SLURM_ARRAY_JOB_ID}.${SLURM_ARRAY_TASK_ID}"
    ).dump(str(out_path))

if __name__ == "__main__":
    for block_count in block_counts:
        write_config(block_count)
        write_batch(block_count)
