#!/usr/bin/env python


import importlib
import itertools
import jinja2
import json
import math
import sys


from pathlib import Path
from typing import Tuple


import constants


config_template_path = Path(__file__).parent.parent / "templates" / "strong-booster.json"
generated_path = Path(__file__).parent.parent / "generated"

templates_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader(Path(__file__).parent.parent / "templates"),
    autoescape=jinja2.select_autoescape()
)


block_count = [4, 4, 3]
block_size = [96, 96, 128]
filling_percents = [
    0,
    10,
    20,
    30,
    40,
    50,
    60,
    70,
    80,
    90,
    100
]


def scale_percent_ceil(x: int, percent: int) -> int:
    """
    Returns x * percent * (1 / 100) rounded up to the next int.
    The return value is at least 0 and at most x.
    """
    return max(0, min(x, math.ceil(x * percent * (1 / 100))))


def divide_ceil(a: int, b: int) -> int:
    return a // b + a % b


def make_label(filling_percent: int) -> str:
    return f"filling-{filling_percent:03}"


def write_config(filling_percent: int) -> None:
    with config_template_path.open(encoding="utf8") as f:
        config = json.load(f)

    config["Geometry"]["blockcount"] = block_count
    config["Geometry"]["blocksize"] = block_size
    config["Settings"]["cuda"]["subblock"]["blockDim"] = [10, 8, 6]

    cells_filling = []
    for z in range(block_count[2]):
        for y in range(block_count[1]):
            for x in range(block_count[0]):
                start = [
                    x * block_size[0],
                    y * block_size[1],
                    z * block_size[2]
                ]
                block_size_z = scale_percent_ceil(block_size[2], filling_percent)
                if block_size_z == 0:
                    continue
                cells_filling.append({
                    "box": [
                        start,
                        [
                            start[0] + block_size[0],
                            start[1] + block_size[1],
                            start[2] + block_size_z
                        ]
                    ],
                    "celltype": 0,
                    "component": 0,
                    "pattern": "const",
                    "seed": 0,
                    "shape": "cube",
                    "value": 0,
                })
    config["Filling"]["cells"] = cells_filling

    out_path = (generated_path / "config" / make_label(filling_percent)).with_suffix(".json")
    print(f"Writing config to {out_path}", file=sys.stderr)
    with out_path.open("w", encoding="utf8") as f:
        json.dump(config, f, indent=2)


def write_batch(filling_percent: int) -> None:
    label = make_label(filling_percent)
    out_path = generated_path / "batch" / label
    config_path = (generated_path / "config" / label).with_suffix(".json")
    print(f"Writing batch to {out_path}", file=sys.stderr)

    t = templates_env.get_template("strong-batch.j2")
    t.stream(
        name=label,
        account="cellsinsilico",
        partition="batch",
        nodes=1,
        tasks=48,
        time="01:00:00",
        nastja_binary_path=constants.NASTJA_NOCUDA_PATH,
        logfile_path="/p/project/cellsinsilico/paulslustigebude/ma/experiments/eval/logs/%x-%A.%a",
        config_path="/p/project/cellsinsilico/paulslustigebude/ma/experiments/eval/generated/config/${SLURM_JOB_NAME}.json",
        output_dir_path="/p/scratch/cellsinsilico/paul/nastja-out/${SLURM_JOB_NAME}-${SLURM_ARRAY_JOB_ID}.${SLURM_ARRAY_TASK_ID}"
    ).dump(str(out_path))

if __name__ == "__main__":
    for filling_percent in filling_percents:
        write_config(filling_percent)
        write_batch(filling_percent)
