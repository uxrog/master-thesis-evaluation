#!/usr/bin/env python


import argparse
import json
import math


def print_table(data, spec):
    print("\t".join(column for column in spec.keys()))

    for data_item in data:
        values = []
        for retrieve in spec.values():
            raw_value = retrieve(data_item)
            values.append(raw_value if isinstance(raw_value, str) else str(raw_value))
        print("\t".join(values))


table_specs = {
    "strong": {
        "label": lambda job: job["accounting"][0]["nodes"]["count"],
        "nodes": lambda job: job["accounting"][0]["nodes"]["count"],
        "tasks": lambda job: job["accounting"][0]["tasks"]["count"],
        "mean_time": lambda job: job["means"]["TimeStep"],
        "std_time": lambda job: job["stds"]["TimeStep"],
        "speedup": lambda job: jobs[0]["means"]["TimeStep"] / job["means"]["TimeStep"],
        # Standard deviation scaled to speedup
        "speedup_std": lambda job: (jobs[0]["means"]["TimeStep"] / job["means"]["TimeStep"]) * (job["stds"]["TimeStep"] / job["means"]["TimeStep"]),
        # 95% confidence interval
        "speedup_error": lambda job: (jobs[0]["means"]["TimeStep"] / job["means"]["TimeStep"]) * (job["stds"]["TimeStep"] / job["means"]["TimeStep"]) / math.sqrt(len(jobs)) * 1.96,
    },
    "weak": {
        "nodes": lambda job: job["accounting"][0]["nodes"]["count"],
        "tasks": lambda job: job["accounting"][0]["tasks"]["count"],
        "mean_time": lambda job: job["means"]["TimeStep"],
        "std_time": lambda job: job["stds"]["TimeStep"],
        "efficiency": lambda job: jobs[0]["means"]["TimeStep"] / job["means"]["TimeStep"],
        "efficiency_error": lambda job: (jobs[0]["means"]["TimeStep"] / job["means"]["TimeStep"]) * (job["stds"]["TimeStep"] / job["means"]["TimeStep"]) / math.sqrt(len(jobs)) * 1.96,
    },
    "subblock": {
        "label": lambda job: job["name"][-8:],
        "x": lambda job: int(job["name"][-8:-6]),
        "y": lambda job: int(job["name"][-5:-3]),
        "z": lambda job: int(job["name"][-2:]),
        "mean_time": lambda job: job["means"]["Sweep:DynamicECM"],
        "speedup": lambda job: jobs[0]["means"]["Sweep:DynamicECM"] / job["means"]["Sweep:DynamicECM"],
    }
}


if __name__ == "__main__":
    p = argparse.ArgumentParser(description="Turn files generated by timing.py into pgf datafiles")
    p.add_argument("timing_file")
    p.add_argument("--variant", choices=["strong", "weak", "subblock"], required=True)
    args = p.parse_args()

    with open(args.timing_file, "r", encoding="utf8") as f:
        jobs = json.load(f)

    scaling_spec = table_specs[args.variant]

    print_table(jobs, scaling_spec)
