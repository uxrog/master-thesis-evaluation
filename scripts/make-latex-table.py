#!/usr/bin/env python


import argparse
import pandas


def show_seconds(s: float) -> str:
    return f"{s:.2f}s"


if __name__ == '__main__':
    p = argparse.ArgumentParser(description="Make a latex table from a timings tsv")
    p.add_argument("timingfile")
    p.add_argument("--weak", action="store_true")
    args = p.parse_args()

    df = pandas.read_csv(args.timingfile, sep="\t")

    for i in range(len(df)):
        if not args.weak:
            print(f"{df['nodes'][i]} & {df['tasks'][i]} & {show_seconds(df['mean_time'][i])} & {show_seconds(df['std_time'][i])} & {df['speedup'][i]:.02f} & {df['speedup_error'][i]:.02f} \\\\")
        else:
            print(f"{df['nodes'][i]} & {df['tasks'][i]} & {show_seconds(df['mean_time'][i])} & {show_seconds(df['std_time'][i])} & {df['efficiency'][i]:.02f} & {df['efficiency_error'][i]:.02f} \\\\")
